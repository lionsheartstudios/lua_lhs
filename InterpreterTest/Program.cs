﻿using System;
using System.IO;
using Language.Lua;

namespace InterpreterTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0 && args[0] == "-f")
            {
                var file = File.ReadAllText(args[1]);
                
                var val = LuaInterpreter.Interpreter(file);
                Console.WriteLine(val);
            }
            else
            {
                var line = "";
                var env = LuaInterpreter.CreateGlobalEnvironment();
                while ((line = Console.ReadLine()) != null)
                {
                    var val = LuaInterpreter.Interpreter(line, env);
                    Console.WriteLine(val);
                }
            }
        }
    }
}