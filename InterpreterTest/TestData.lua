﻿Item={
    Learn_From_Barnabas={
        Name="Learn From Barnabas",
        State="active",
        Entry_1="[Optional] Win a smile competition",
        Entry_1_State="active"
    },
    Talk_With_Leah={
        Name="Talk With Leah",
        State="unassigned"
    },
    Make_Leah_Happy={
        Name="Make Leah Happy",
        State="unassigned"
    },
    Make_Leah_Really_Happy={
        Name="Make Leah Really Happy",
        State="unassigned"
    },
    Talk_With_Issac={
        Name="Talk With Issac",
        State="unassigned"
    },
    Give_Isaac_Blueberries={
        Name="Give Isaac Blueberries",
        State="unassigned"
    },
    Challenge_Your_Diagram_Brain={
        Name="Challenge Your Diagram Brain",
        State="unassigned"
    }
};

Item["This_Quest_is_success"].State = "success";

return Item["This_Quest_has_entries"].Entry_4_State